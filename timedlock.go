/*
timedlock is a package that prevents deadlocking through using a
timeout. The HasLock method will attempt to aquire the lock and will return
whether or not the lock was aquired or if it timed out.
*/
package timedlock

import (
	"context"
	"log"
	"sync"
	"time"
)

// TimedLock tries to call lock with the locker
type TimedLock struct {
	sync.Locker
	Timeout time.Duration
}

// HasLock attempts to aquire the lock. It is succeeds in aquiring the lock
// before the timeout true is returned, otherwise false is returned.
func (t TimedLock) HasLock() (success bool) {
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	ctx, cancel = context.WithTimeout(context.Background(), t.Timeout)
	defer cancel()
	return getLock(ctx, t.Locker)

}

func getLock(ctx context.Context, l sync.Locker) (success bool) {
	var (
		// Give cancel a buffer of one in case lock() is still trying to aquire
		// the lock
		cancel = make(chan struct{}, 1)
	)
	defer close(cancel)

	select {
	case <-lock(l, cancel):
		return true
	case <-ctx.Done():
		cancel <- struct{}{}
		return false
	}

}

func lock(locker sync.Locker, canceled <-chan struct{}) <-chan bool {
	locked := make(chan bool)
	go func() {
		locker.Lock()
		select {
		case <-canceled:
			log.Println("Read from canceled")
			locker.Unlock()
		case locked <- true:
			log.Println("Wrote to locked")
		default:
		}
		close(locked)

	}()
	return locked
}
